'use strict';
// Loads path to access helper functions for working with files and directory paths
var path = require("path");
var express = require("express");

module.exports = function(app, database) {

    // Defines paths
    // __dirname is a global that holds the directory name of the current module
    const CLIENT_FOLDER = path.join(__dirname + '/../client');
    const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');

    // MIDDLEWARES --------------------------------------------------------------------------------------------------------
    // Serves files from public directory (in this case CLIENT_FOLDER).
    // __dirname is the absolute path of the application directory.
    // if you have not defined a handler for "/" before this line, server will look for index.html in CLIENT_FOLDER
    app.use(express.static(CLIENT_FOLDER));

    // ROUTE HANDLERS -----------------------------------------------------------------------------------------------------
    // Defines endpoint exposed to client side for registration
    app.post("/api/users", function (req, res, next) {
        // Information sent via an HTTP POST is found in req.body
        console.log('\nInformation submitted to server:')
        console.log(req.body);

        database.connection
            .transaction(function (t) {
                return database.User
                    .create(
                        {
                            user_id: req.body.users.user_id,
                            username: req.body.users.username,
                            email: req.body.users.email,
                            firstName: req.body.users.firstName,
                            lastName: req.body.users.lastName,
                            dob: new Date(req.body.users.dob),
                            gender: req.body.users.gender,
                            joindate: new Date(req.body.users.joindate),
							photo: req.body.users.photo
                        }
                        , {transaction: t})
                    .then(function (user) {
                        console.log("inner result " + JSON.stringify(user))
                        return database.POI
                            .create(
                                {
                                    poi_id: req.body.user.poi_id
                                    , user_id: req.body.user.user_id
                                }
                                , {transaction: t});
                    });
            })
            .then(function (user) {
                res
                    .status(200)
                    .json(user);
            })
            .catch(function (err) {
                console.log(err);
                res
                    .status(501)
                    .json(err);
            });
    });

    // Defines endpoint handler exposed to client side for retrieving user information from database. Client side
    // sent data as part of the query string, we access query string paramters via the req.query property
    app.get("/api/users", function (req, res) {
        database.User
        // findAll asks sequelize to retrieve multiple records (all records if where clause not used, i.e., no filtering)
            .findAll({
                where: {
                    // This where condition filters the findAll result so that it only includes user names and
                    // user numbers that have the searchstring as a substring (e.g., if user entered 's' as search
                    // string, the following
                    $or: [
                        {firstName: {$like: "%" + req.query.searchString + "%"}},
                        {lastName: {$like: "%" + req.query.searchString + "%"}},
                        {user_id: {$like: "%" + req.query.searchString + "%"}}
                    ]
                }
                // We add a limit since users table is big
                , limit: 100
            })
            .then(function (users) {
                res
                    .status(200)
                    .json(users);
            })
            .catch(function (err) {
                res
                    .status(500)
                    .json(err);
            });
    });


    /* Defines endpoint handler exposed to client side for retrieving user records that match query string passed.
    Match against dept name and dept no. Includes manager information. Client side sent data as part of the query
    string, we access query string paramters via the req.query property
    */
    app.get("/api/users/poi", function (req, res) {
        database.User
        // Use findAll to retrieve multiple records
            .findAll({
                // Use the where clause to filter final result; e.g., when you only want to retrieve users that have
                // "s" in its name
                where: {
                    // $or operator tells sequelize to retrieve record that match any of the condition
                    $or: [
                        // $like + % tells sequelize that matching is not a strict matching, but a pattern match
                        // % allows you to match any string of zero or more characters
                        {firstName: {$like: "%" + req.query.searchString + "%"}},
                        {lastName: {$like: "%" + req.query.searchString + "%"}},
                        {user_id: {$like: "%" + req.query.searchString + "%"}}
                    ]
                }
                , limit: 100
                // What Include attribute does: Join two or more tables. In this instance:
                // 1. For every User record that matches the where condition, the include attribute returns
                // ALL users that have served as managers of said User
                // 2. model attribute specifies which model to join with primary model
                // 3. order attribute specifies that the list of Managers be ordered from latest to earliest manager
                // 4. limit attribute specifies that only 1 record (in this case the latest manager) should be returned
                , include: [{
                    model: database.PoiUser
                    , order: [["to_date", "DESC"]]
                    , limit: 1
                    // We include the User model to get the manager's name
                    , include: [database.POI]
                }]
            })
            // this .then() handles successful findAll operation
            // in this example, findAll() used the callback function to return users
            // we named it userss, but this object also contains info about the
            // latest department of that user
            .then(function (users) {
                res
                    .status(200)
                    .json(users);
            })
            // this .catch() handles erroneous findAll operation
            .catch(function (err) {
                res
                    .status(500)
                    .json(err);
            });
    });


    // -- Searches for specific users by user_id

    app.get("/api/users/:user_id", function (req, res) {
        console.log
        var where = {};
        if (req.params.user_id) {
            where.user_id = req.params.user_id
        }

        console.log("where " + where);
        // We use findOne because we know (by looking at the database schema) that user_id is the primary key and
        // is therefore unique. We cannot use findByNo because findByNo does not support eager loading
        database.User
            .findOne({
                where: where
                , include: [{
                    model: database.Poi_User
                    , order: [["to_date", "DESC"]]
                    , limit: 1
                    // We include the User model to get the manager's name
                    , include: [database.Department]
                }]
            })

            .then(function (users) {
                console.log("-- GET /api/users/:user_id findOne then() result \n " + JSON.stringify(users));
                res.json(users);
            })
            // this .catch() handles erroneous findAll operation
            .catch(function (err) {
                console.log("-- GET /api/users/:user_id findOne catch() \n ");

                res
                    .status(500)
                    .json({error: true});
            });
    });

    // -- Updates users
    app.put('/api/users/:user_id', function (req, res) {
        var where = {};
        where.user_id = req.params.user_id;

        // Updates user detail
        console.log("body " + JSON.stringify(req.body));
        database.User
            .update(
                {firstName: req.body.firstName}
                , {where: where}                            // search condition / criteria
            )
            .then(function (user) {
                res
                    .status(200)
                    .json(user);
            })
            .catch(function (err) {
                console.log(err);
                res
                    .status(500)
                    .json(err);
            });
    });


    // -- Searches for and deletes user of a specific user num
    app.delete("/api/users/:user_id", function (req, res) {
        var where = {};
        where.user_id = req.params.user_id;

        database.User
            .destroy({
                where: where
            })
            .then(function (result) {
                if (result == "1")
                    res.json({success: true});
                else
                    res.json({success: false});
            })
            .catch(function (err) {
                console.log("-- DELETE /api/users/:user_id catch(): \n" + JSON.stringify(err));
            });
    });

    // Defines endpoint exposed to client side for retrieving all user information (STATIC)
    app.get("/api/static/users", function (req, res) {
        var users = [
            {
                userNo: 1001,
                userFirstName: 'Emily',
                userLastName: 'Smith',
				userEmail: 'emilysmith@guru.com'
            }
            , {
                userNo: 1002,
                userFirstName: 'Varsha',
                userLastName: 'Jansen',
				userEmail: 'varshajansen@guru.com'
            }
            , {
                userNo: 1003,
                userFirstName: 'Julie',
                userLastName: 'Black',
				userEmail: 'julieblack@guru.com'
            }

        ];
        // Return users as a json object
        res
            .status(200)
            .json(users);
    });

    app.get("/api/poi", function (req, res) {
        database.connection
        // Explanation of SQL statement
        // 1. SELECT - SELECT specifies that this is a read/retrieve command
        // 2. poi_id, ... - identifies the columns to return; use * to return all columns
        // 3. FROM poi - specifies the table to read data from
            .query("SELECT poi_id, destid" +
                "FROM poi "
            )
            // this .spread() handles successful native query operation
            // we use .spread instead of .then so as to separate metadata from the emplooyee records
            .spread(function (poi) {
                res
                    .status(200)
                    .json(poi);
            })
            // this .catch() handles erroneous native query operation
            .catch(function (err) {
                res
                    .status(500)
                    .json(err);
            });
    });

    // Handles 404. In Express, 404 responses are not the result of an error,
    // so the error-handler middleware will not capture them.
    // To handle a 404 response, add a middleware function at the very bottom of the stack
    // (below all other path handlers)
    app.use(function (req, res) {
        res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
    });

    // Error handler: server error
    app.use(function (err, req, res, next) {
        res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
    });

};

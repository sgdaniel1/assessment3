// Loads sequelize ORM
var Sequelize = require("sequelize");

var config = require("./config");
// Defines MySQL configuration
//const MYSQL_USERNAME = 'root';
//const MYSQL_PASSWORD = 'password';
//console.log(config.mysql);
// DBs, MODELS, and ASSOCIATIONS ---------------------------------------------------------------------------------------
// Creates a MySQL connection
var connection = new Sequelize( config.mysql,
    {
        logging: console.log,
		port: 3306,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);
console.log(connection);

var User = require('./models/users')(connection, Sequelize);
var POI = require('./models/poi')(connection, Sequelize);
var PoiUser = require('./models/poiuser')(connection, Sequelize);

User.hasMany(PoiUser, {foreignKey: 'user_id'})
//PoiUser.hasOne(POI, {foreignKey: 'poi_id'});
PoiUser.belongsTo(POI, {foreignKey: 'poi_id'});

module.exports = {
    User: User,
    POI: POI,
    PoiUser: PoiUser,
    connection: connection,
};

module.exports = function (conn, Sequelize) {
    var Destination = conn.define("destinations",
        {
            dest_id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                allowNull: false
            },
            city: {
                type: Sequelize.STRING,
                allowNull: false
            },
            country: {
                type: Sequelize.STRING,
                allowNull: false
            },
            nearAirport: {
                type: Sequelize.STRING,
                allowNull: false
            },
            longi: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            lat: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            photo: {
                type: Sequelize.STRING,
                allowNull: false
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false
         });

    return Destination;
};

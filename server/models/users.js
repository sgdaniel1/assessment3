
module.exports = function(conn, Sequelize) {
    var Users=  conn.define('users', {
        user_id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        username: {
            type: Sequelize.STRING,
            allowNull: false
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false
        },
        firstName: {
            type: Sequelize.STRING,
            allowNull: false
        },
        lastName: {
            type: Sequelize.STRING,
            allowNull: false
        },
        gender: {
            type: Sequelize.ENUM('M','F'),
            allowNull: false
        },
        dob: {
            type: Sequelize.DATE,
            allowNull: false
        },
        joindate: {
            type: Sequelize.DATE,
            allowNull: false
        },
        photo: {
            type: Sequelize.STRING,
            allowNull: false
        }
    }, {
        tableName: 'users',
        timestamps: false
    });
    return Users;
};

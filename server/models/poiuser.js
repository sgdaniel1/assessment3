module.exports = function (conn, Sequelize) {
    var PoiUser = conn.define("poi_user",
        {
            user_id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                allowNull: false,
                references: {
                    model: 'users',
                    key: 'user_id'
                }
            },
            poi_id: {
                type: Sequelize.STRING,
                primaryKey: true,
                allowNull: false,
                references: {
                    model: 'poi',
                    key: 'poi_id'
                }
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false
            , tableName: 'poi_user'
         });

    return PoiUser;
};

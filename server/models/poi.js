module.exports = function (conn, Sequelize) {
    var POI = conn.define("poi",
        {
            poi_id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                allowNull: false
            },
            dest_id: {
                type: Sequelize.INTEGER(11),
                allowNull: false
			},
            img_poi: {
                type: Sequelize.STRING,
                allowNull: false
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false
         });

    return POI;
};

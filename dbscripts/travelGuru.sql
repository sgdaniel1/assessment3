DROP DATABASE IF EXISTS travelGuru;
CREATE DATABASE IF NOT EXISTS travelGuru;
USE travelGuru;

SELECT 'CREATING DATABASE STRUCTURE' as 'INFO';

DROP TABLE IF EXISTS destination,
                     POI,
                     users,
                     poi_user, 
                     reviews
;
                     
CREATE TABLE destination(
    destid int(11) not null auto_increment primary key,
    city varchar(25) not null,
	country varchar(25) not null,
    nearAirport varchar(25) not null,
    longti int(11) not null,
    lat int(11) not null,
    photo varchar(255) not null
    ) ENGINE = InnoDB;
    
CREATE TABLE POI(
	poi_id int(11) not null auto_increment primary key,
    destid int(11) not null,
    img_poi varchar(255)not null,
    review_id int(11) not null,
    FOREIGN KEY fk_destination(destid) REFERENCES destination(destid)
	on update cascade
    on delete restrict
    ) ENGINE = InnoDB;


CREATE TABLE users(
    user_id int(11) not null auto_increment primary key,
    username varchar(25) not null,
    email varchar(25) not null,
	firstName varchar(25) not null,
    lastName varchar(25) not null,
    dob date not null,
	gender enum('M','F') not null,
    joindate date not null,
    photo varchar(255)not null
    ) ENGINE = InnoDB;
    
CREATE TABLE poi_user(
	poiid_user int(11) not null auto_increment primary key,
    user_id int(11) not null,
    poi_id int(11) not null,
    FOREIGN KEY fk_users(user_id) REFERENCES users(user_id),
    FOREIGN KEY fk_poi(poi_id) REFERENCES poi(poi_id)
    on update cascade
    on delete restrict
    ) ENGINE = InnoDB;
    
CREATE TABLE reviews(
	review_id int(11) not null auto_increment primary key,
    rating varchar(255) not null,
    review varchar(255) not null,
    FOREIGN KEY fk_review(review_id) REFERENCES poi(destid)
	on update cascade
    on delete restrict
    ) ENGINE = InnoDB;

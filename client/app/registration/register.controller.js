//TODO: Add ability to choose poi when registering an user.-->
//TODO: Saving of user in users table and of poi in pois table should be-->
//TODO: one  transaction (i.e., atomic)-->
//TODO: 2. Allow users to choose a poi from a list of users. Note: Function to call is in DeptService


// Always use an IIFE, i.e., (function() {})();
(function() {
    angular
        .module("TravelGuru")          // to call an angular module, omit the second argument ([]) from the angular.module() syntax
        // this syntax is called the getter syntax
        .controller("RegCtrl", RegCtrl);    // angular.controller() attaches a controller to the angular module specified
                                            // as you can see, angular methods are chainable

    // TODO: 2.1 Inject DeptService so we can access appropriate function, Inject $filter because we need it to format dates
    RegCtrl.$inject = [ '$window', 'UserService', 'PoiService' ];

    function RegCtrl( $window, UserService, PoiService) {

        // Declares the var vm (for ViewModel) and assigns it the object this (in this case, the RegCtrl)
        // Any function or variable that you attach to vm will be exposed to callers of RegCtrl, e.g., index.html
        var vm = this;
        var today = new Date();
        var birthday = new Date();
        birthday.setFullYear(birthday.getFullYear() - 18);

        // Exposed data models ---------------------------------------------------------------------------------------
        // Creates an user object that
        // We expose the user object by attaching it to the vm
        // This will allow us apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        vm.user = {
            user_id: "",
            username: "",
            email: "",
            firstName: "",
            lastName: "",
            gender: "M",
            dob: birthday,
            joindate: today,
            photo: ""
        };

        // Creates a status object. We will use this to display appropriate success or error messages.
        vm.status = {
            message: "",
            code: ""
        };


        vm.register = register;

        // TODO: 2.2 Create function that would populate the poi selection box with data. Logic should be placed
        // TODO: 2.2 in DeptService, but handling of success/error should be done in this controller

        initPoiBox();

        // Exposed functions -----------------------------------------------------------------------------------------

        function initPOIBox() {
            PoiService
                .retrievePoi()
                .then(function (results) {
                    console.log("--- Poi ----");
                    console.log(results.data);
                    vm.poi = results.data;
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    vm.status.code = err.data.parent.errno;
                });
        }

        // Function declaration and definition
        function register() {
            // Calls alert box and displays registration information
            alert("The registration information you sent are \n" + JSON.stringify(vm.user));

            // Prints registration information onto the client console
            console.log("The registration information you sent were:");
            console.log("User Number: " + vm.user.user_id);
            console.log("User Login Name: " + vm.user.username);
            console.log("User Email: " + vm.user.email);
            console.log("User First Name: " + vm.user.firstName);
            console.log("User Last Name: " + vm.user.lastName);
            console.log("User Gender: " + vm.user.gender);
            console.log("User Birthday: " + vm.user.dob);
            console.log("User Join Date: " + vm.user.joindate);
            console.log("User Photo: " + vm.user.photo);


            // We call UserService.insertUser to handle registration of user information. The data sent to this
            // function will eventually be inserted into the database.
            UserService
                .insertUser(vm.user)
                .then(function (result) {
                    console.log("result " + JSON.stringify(result));
                    $window.location.assign('/app/registration/thanks.html');
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    vm.status.message = err.data.name;
                    vm.status.code = err.data.parent.errno;
                });

        } // END function register()
    } // END RegCtrl
})();

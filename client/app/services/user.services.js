
// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches UserService service to the TravelGuru module
    angular
        .module("TravelGuru")
        .service("UserService", UserService);

    // Dependency injection. Here we inject $http because we need this built-in service to communicate with the server
    // There are different ways to inject dependencies; $inject is minification safe
    UserService.$inject = ['$http'];

    // UserService function declaration
    // Accepts the injected dependency as a parameter. We name it $http for consistency, but you may assign any name
    function UserService($http) {

        // Declares the var service and assigns it the object this (in this case, the UserService). Any function or
        // variable that you attach to service will be exposed to callers of UserService, e.g., search.controller.js
        // and register.controller.js
        var service = this;

        // EXPOSED DATA MODELS -----------------------------------------------------------------------------------------
        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
        service.insertUser = insertUser;
        service.retrieveUser = retrieveUser;
        service.retrieveUserByUser_id = retrieveUserByUser_id;
        service.retrieveUserDB = retrieveUserDB;
        service.updateUserName = updateUserName ;
        service.deleteUser = deleteUser;

        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------
        // TODO: 4. Create function for deleting user; search by  user_id
        // deleteDept uses HTTP DELETE to delete department from database; passes information as route parameters.
        // IMPORTANT! Route parameters are not the same as query strings!
        function deleteUser(user_id) {
            return $http({
                method: 'DELETE'
                , url: 'api/users/' +  user_id
            });

        }


        // insertUser uses HTTP POST to send user information to the server's /users route
        // Parameters: user information; Returns: Promise object
        function insertUser(user) {
            // This line returns the $http to the calling function
            // This configuration specifies that $http must send the user data received from the calling function
            // to the /users route using the HTTP POST method. $http returns a promise object. In this instance
            // the promise object is returned to the calling function
            return $http({
                method: 'POST'
                , url: 'api/users'
                , data: {emp: user}
            });
        }

        // retrieveUser retrieves user information from the server via HTTP GET.
        // Parameters: none. Returns: Promise object
        function retrieveUser(){
            return $http({
                method: 'GET'
                , url: 'api/static/users'
            });
        }

        // TODO: 4. Create function for retrieving specific emp; search by emp no
        function retrieveUserByUser_id(user_id) {
            return $http({
                method: 'GET'
                , url: 'api/users/' + user_id
            });
        }


        // retrieveUserDB retrieves user information from the server via HTTP GET. Passes information via the query
        // string (params) Parameters: searchString. Returns: Promise object
        function retrieveUserDB(searchString){
            return $http({
                method: 'GET'
                , url: 'api/users'
                , params: {
                    'searchString': searchString
                }
            });
        }

        // retrieveUserDest retrieves user and department information from the server via HTTP GET.
        // Parameters: searchString. Returns: Promise object
        function retrieveUserDest(searchString){
            return $http({
                method: 'GET'
                , url: 'api/users/destinations'
                , params: {
                    'searchString': searchString
                }
            });
        }

        // TODO: 4. Create function for updating emp first name of specific emp; search by emp no
        function updateUserName( user_id, firstName) {
            return $http({
                method: 'PUT'
                , url: 'api/users/' + user_id
                , data: {
                    firstName: firstName
                }
            });
        }


    }
})();

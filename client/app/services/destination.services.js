// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches DestService service to the DMS module
    angular
        .module("TravelGuru")
        .service("DestService", DestService);

    // Dependency injection. Here we inject $http because we need this built-in service to communicate with the server
    // There are different ways to inject dependencies; $inject is minification safe
    DestService.$inject = ['$http'];

    // DestService function declaration
    // Accepts the injected dependency as a parameter. We name it $http for consistency, but you may assign any name
    function DestService($http) {

        // Declares the var service and assigns it the object this (in this case, the DestService). Any function or
        // variable that you attach to service will be exposed to callers of DestService, e.g., search.controller.js
        // and register.controller.js
        var service = this;

        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
        service.retrieveDestinations = retrieveDestinations;

        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------
        function retrieveDestinations() {
            return $http({
                method: 'GET'
                , url: 'api/destinations'
            });
        }
    }
})();

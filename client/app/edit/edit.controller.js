// TODO: Search(specific), Update, Delete
// TODO: 3. Build controller for edit html. Should support functionalities listed in edit.html
// Always use an IIFE, i.e., (function() {})();

(function () {

    angular
        .module("TravelGuru")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$filter","$stateParams", "UserService"];

    function EditCtrl($filter, $stateParams, UserService) {

        // Declares the var vm (for ViewModel) and assigns it the object this. Any function or variable that you attach
        // to vm will be exposed to callers of EditCtrl, e.g., edit.html
        var vm = this;

        // Exposed data models -----------------------------------------------------------------------------------------
        // Creates a destination object. We expose the destination object by attaching it to the vm. This allows us to
        // apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        vm.user_id = "";
        vm.result = {};

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
        vm.deleteUser = deleteUser;
        vm.initDetails = initDetails;
        vm.updateUserName = updateUserName;
        vm.search = search;
        vm.toggleEditor = toggleEditor;


        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        initDetails();
        console.log("before state");
        if($stateParams.user_id){
            console.log("$stateparams " + $stateParams.user_id);
            vm.user_id = Number($stateParams.user_id);
             vm.search();
        }


        // Function declaration and definition -------------------------------------------------------------------------
        // Deletes displayed manager. Details of preceding manager is then displayed.
        function deleteUser() {
            UserService
                .deleteUser(vm.user_id)
                .then(function (response) {
                    // Calls search() in order to populate manager info with predecessor of deleted manager
                    search();
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error: \n" + JSON.stringify(err));
                });
        }

        // Initializes destination details shown in view
        function initDetails() {
            console.log("-- show.controller.js > initDetails()");
            vm.result.user_id = "";
            vm.result.firstName = "";
            vm.result.lastName = "";
            vm.showDetails = false;
            vm.isEditorOn = false;

        }

        // Saves edited destination name
        function updateUserName() {
            console.log("-- show.controller.js > updateUserFirstName()");
            UserService
                .updateUserName( vm.result.user_id, vm.result.firstName, vm.result.lastName)
                .then(function (result) {
                    console.log("-- show.controller.js > updateUserFirstName() > results: \n" + JSON.stringify(result.data));
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > updateUserFirstName() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }


        // Given a destination number, this function searches the User database for
        // the destination name, and the latest destination manager's id/name and tenure
        function search() {
            console.log("-- show.controller.js > search()");
            initDetails();
            vm.showDetails = true;

            UserService
                .retrieveUserByUser_id(vm.user_id)
                .then(function (result) {
                    // Show table structure
                    vm.showDetails = true;

                    // This is a good way to understand the type of results you're getting
                    console.log("-- show.controller.js > search() > results: \n" + JSON.stringify(result.data));

                    // Exit .then() if result data is userty
                    if (!result.data)
                        return;

                    // The result is an array of objects that contain only 1 object
                    // We are assigning value like so, so that we don't have to do access complex structures
                    // from the view. Also this would give you a good sense of the structure returned.
                    // You could, of course, massage data from the back end so that you get a simpler structure
                    vm.result.user_id = result.data.user_id;
                    vm.result.firstName = result.data.firstName;
                    vm.result.lastName = result.data.lastName;
                    //vm.result.userdept_no = result.data.dept_users[0].dept_no;
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > search() > error: \n" + JSON.stringify(err));
                });

        }
        // Switches editor state of the destination name input/edit field
        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }

    }
})();

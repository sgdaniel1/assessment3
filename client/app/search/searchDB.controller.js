(function () {
    angular
        .module("EMS")
        .controller("SearchDBCtrl", SearchDBCtrl);

    SearchDBCtrl.$inject = ['$state','UserService'];

    function SearchDBCtrl($state, UserService) {
        var vm = this;

        vm.searchString = '';
        vm.result = null;
        vm.showDestination = false;

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
        vm.goEdit = goEdit;
        vm.search = search;
        vm.searchForDestination = searchForDestination;

        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        // init is a private function (i.e., not exposed)
        init();



        // Function declaration and definition -------------------------------------------------------------------------
        function goEdit(empNo){
            $state.go("editWithParam",{empNo : empNo});
        }

        // The init function initializes view
        function init() {
            // We call UserService.retrieveUserDB to handle retrieval of destination information. The data retrieved
            // from this function is used to populate search.html. Since we are initializing the view, we want to
            // display all available destinations, thus we ask service to retrieve '' (i.e., match all)
            UserService
                .retrieveUserDB('')
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    vm.users = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
        }

        // The search function searches for users that match query string entered by user. The query string is
        // matched against the user first name, last name, and user number.
        function search() {
            vm.showDestination = false;
            UserService
            // we pass contents of vm.searchString to service so that we can search the DB for this string
                .retrieveUserDB(vm.searchString)
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    vm.users = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
        }

        // The search function searches for destinations that matches query string entered by user. The query string is
        // matched against the user name and user number alike.
        function searchForDestination() {
            vm.showDestination = true;
            UserService
            // we pass contents of vm.searchString to service so that we can search the DB for this string
                .retrieveUserDest(vm.searchString)
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    console.log("results: " + JSON.stringify(results.data));
                    vm.users = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.info("error " + JSON.stringify(err));
                });
        }
    }
})();
